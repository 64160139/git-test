public class New_array {
    public static void main(String[] args) {
        // การสร้าง array
        /* แบบกำหนดค่าเอาไว้
        int [] num1 = new int[4];
        num1[0] = 1;
        num1[1] = 2;
        num1[2] = 3;
        num1[3] = 4;
        System.out.println(num1[0]);
        */

        /*
        แบบกำหนดสมาชิก
        int [] numx = {10,20,30,40,50};
        System.out.println(numx[0]);
        numx[4] = 200 ; // เปลี่ยนค่าใน Array
        System.out.println(numx[4]);
        */
        
        // การนับ Array
        // int [] numx = {10,20,30,40,50};
        // System.out.println(numx.length);
        // เก็บนับไว้ในตัวแปร
        // int n = numx.length;
        // System.out.println(n);

        int [] numx = {10,20,30,40,50};
        int n = numx.length;
        
        /*for(int i = 0 ; i < n ; i++){
            System.out.println(numx[i]);
        }
        */
        int sumx = 0;
        for(int item : numx){
            sumx = sumx + item;
            System.out.println("ค่าที่ = "+item);
            
        }
        System.out.println("ผลรวม = "+sumx);
    }
}
