public class LoopD1 {
    public static void main(String[] args) {
        /*
        while loop
        int count = 1;
        while (count <=10){
            System.out.println("รอบที่ = "+count+" Hello ");
            count = count +1;
        } 
        */
        /*
        for loop
        มี 3 ส่วน
        1.) ส่วนตัวแปรเริ่มต้น => ตัวนับรอบ
        2.) เงื่อนไข
        3.) การเพิ่มค่า / ลดค่า 
        for(ส่วนตัวแปรเริ่มต้น  ; เงื่อนไข ; การเพิ่มค่า / ลดค่า){}
        */
        
        for(int num1 = 1 ; num1 <=10 ; num1++  ){
            // คำสั่งซ้ำ
            System.out.println("Fah");
            
        }
    }
}
