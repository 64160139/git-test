public class String_2 {
    public static void main(String[] args) {
        String massage = "Happy birth day";
        // massage = massage.replace("day","fah"); เปลี่ยนคำที่เราต้องการเปลี่ยน
        // massage = massage.replaceFirst("day","fah"); เปลี่ยนแค่คำแรก

        // String => Array ต้องเป็นสัญลักษณ์
        // String f = "มะม่วง,กล้วย,แตงโม";
        /*String [] fruit = f.split(",");
        System.out.println(fruit[3]);*/

        // String word = massage.substring(0,3); อ
        // System.out.println(word);

        // char [] name = massage.toCharArray(); เปลี่ยน string เป็น array
        // System.out.println(name[0]);

        // char [] name = {'f','a','h'}; เปลี่ยน Array เป็น string
        // String m = String.copyValueOf(name);
        // System.out.println(m);

        // int num = 100;
        // String numx = String.valueOf(num);
        // System.out.println(numx);
  
        // massage = massage.trim(); ตัดช่องว่าง
        // System.out.println(massage);

        // String b = massage.toUpperCase();
        // String x = massage.toLowerCase();
        // System.out.println(b);
    }
}
