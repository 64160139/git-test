import java.util.Scanner ;
class Input20 {
    public static void main(String[] args) {

        // ประกาศใช้งาน class | new
        Scanner lk=new Scanner(System.in);

        System.out.print("ชื่อ = ");
        String name =lk.nextLine(); //รับ ค่าstring จากผู้ใช้

        
        
        System.out.print("กรอก พ.ศ. = ");
        int num =lk.nextInt();  //รับ ค่า Integer จากผู้ใช้

        System.out.print("กรอกอายุ = ");
        int age =2564 - num; // หาอายุจากการลบของ 2564 - num

        System.out.println("ชื่อของคุณคือ ="+name);
        System.out.println("พ.ศ. = "+num);
        System.out.println("อายุของคุณคือ  ="+age);
    }
}
