import java.util.Scanner;
class Control {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);

        System.out.print("ป้อนตัวเลข = ");
        int a = sc.nextInt();
        if(a>=15 && a<=19){
            System.out.println("วัยรุ่น");
        }
        else if(a>=20 && a<=29){
            System.out.println("วัยหนุ่ม / สาว");
        }
        else if(a>=30 && a<=49){
            System.out.println("วัยทำงาน");
        }
        else if(a>=60){
            System.out.println("วัยชรา");
        }
        System.out.println("จบโปรแกรม");
    }
}
