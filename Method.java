public class Method {
    public static void main(String[] args) {
        //oji(10,20); // การเรียกใช้ method
        //name("fah","love");
        int sumx =  max(50,10);
        System.out.println("น้อยที่สุดคือ = "+sumx);
    }
    // การสร้าง method
    static void  moji(int n , int x){
        System.out.println("คำนวน = "+(n+x));
    }
    static void name(String a , String m){
        System.out.println("ชื่อ "+a+" นามสกุล "+m);
    }
    // ไม่รับ แต่ ส่งค่ากลับ
    static int mo(){    
        int a = 100 ;
        int k = 500 ;
        a+=k;
        return a ;
    }
    // รับและส่งค่ากลับ
    static int max(int a , int b){
        int min = 0;
        if(a < b){
            min = a ;
        }else{
            min = b ;
        }
        return min ;
    }
}
