import java.util.Scanner;
public class Exception_1 {
    public static void main(String[] args) { // ArithmeticException //ArrayIndexOutOfBoundsException
        try{
            //int a = 10;
            //int b = 0;
            //int c = a/b;
            //System.out.print(c);
            //int [] num = {10,20,30,40,50};
            Scanner dd = new Scanner(System.in);
            System.out.print("ป้อนตัวเลข = ");
            int num = dd.nextInt();
            System.out.println(num);
        }
        catch(ArithmeticException s){
            System.out.println("NO");

        }
        catch(ArrayIndexOutOfBoundsException e){
            System.out.println("NO...");
        }
        catch(Exception a){
            System.out.println("กรอกให้ถูกต้อง");
        }
    }
}
