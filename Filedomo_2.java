import java.io.*; 
public class Filedomo_2 {  // วิธีอ่าน ไฟล์ ที่บันทึก
    public static void main(String[] args) {
        try{
            FileReader reader = new FileReader(new File("test.txt"));
            BufferedReader br = new BufferedReader(reader);
            String message = " ";

            while((message = br.readLine()) != null){
                System.out.println(message);
            }
            
        }
        catch(Exception e){
            e.printStackTrace();
        }
    }
}