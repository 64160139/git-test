import java.util.Scanner;
public class minmax {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        int maxx = 0,minx = Integer.MAX_VALUE ;
        while(true){
            System.out.print("ป้อนตัวเลข = ");
            int num1 = sc.nextInt();
            if(num1 < 0) break;

            if(num1 > maxx){
                maxx = num1;

            }
            if(num1 < minx){
                minx =  num1 ;
            }
        }
        System.out.println("ตัวเลขมากที่สุด = "+maxx);
        System.out.println("ตัวเลขน้อยที่สุด = "+minx);
    }
}
