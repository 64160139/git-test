import java.util.Scanner;
public class D3a {
    public static void main(String[] args) {
        Scanner sc =new Scanner(System.in);
        System.out.print("ป้อนอาการของโรค : ");

        String month = sc.nextLine();
        String name;

        switch (month){
            case "ปวดหัว":
                name = "ยาพารา";
                break ;
            case "ปวดท้อง":
                name = "ยาธาตุ";
                break;
            case "ปวดกล้ามเนื้อ":
                name = "นอนพัก";
                break;
            default :
                name = "ไปหาหมอเถอะอีสัส";
                break ;

        }
        System.out.println(name);

    }
}
