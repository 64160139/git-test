import java.util.Scanner;
public class Bank {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        System.out.print("ป้อนจำนวนเงิน : ");

        int money = sc.nextInt();
        System.out.println("จำนวนเงิน = "+money);
        System.out.println("--------------------------------------------");

        if(money >= 1000){
            System.out.println("จำนวนแบงค์ 1000 บาท "+(money/1000)+" ใบ ");
            money = money % 1000;
        }
        if(money >=500){
            System.out.println("จำนวนแบงค์ 500 บาท "+(money/500)+" ใบ ");
            money = money % 500;
        }
        if(money >=100){
            System.out.println("จำนวนแบงค์ 100 บาท "+(money/100)+" ใบ ");
            money = money % 100;
        }
        if(money >=50){
            System.out.println("จำนวนแบงค์ 50 บาท "+(money/50)+" ใบ ");
            money = money % 50;
        }
        if(money >=20){
            System.out.println("จำนวนแบงค์ 20 บาท "+(money/20)+" ใบ ");
            money = money % 20;
        }
        if(money >=10){
            System.out.println("จำนวนเหรียญ 10 บาท "+(money/10)+" เหรียญ ");
            money = money % 10;
        }
        if(money >=5){
            System.out.println("จำนวนเหรียญ 5 บาท "+(money/5)+" เหรียญ ");
            money = money % 5;
        }
        if(money >=2){
            System.out.println("จำนวนเหรียญ 2 บาท "+(money/2)+" เหรียญ ");
            money = money % 2;
        }
        if(money >=1){
            System.out.println("จำนวนเหรียญ 1 บาท "+(money/1)+" เหรียญ ");
            money = money % 1;
        }
        System.out.println("--------------------------------------------");
    }
}
