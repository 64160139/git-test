import java.util.Scanner;
class Operater {
    public static void main(String[] args) {
        
        Scanner sc=new Scanner(System.in);
        int sum = 0, count = 0;
        while(true){
            System.out.print("ป้อนตัวเลข (ป้อนตัวเลขติดลบจะหลุดออกจาก loop) = ");
            int n = sc.nextInt();
            if(n<0) break;
            sum = sum +n ;
            count++;
        }
        System.out.println("ผลรวม = "+sum);
        System.out.println("จำนวนรอบ = "+count);
    }
    
}
