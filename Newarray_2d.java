public class Newarray_2d {
    public static void main(String[] args) {
        String [][] list1 = {
            {"A","B","C"},
            {"D","E","F"},
            {"G","H","I"}
        } ;
        // list1[เลขแถว][เลขคอลัม]
        //System.out.println(list1[0][0]);
        //list1[0][0] = "a";
        //System.out.println(list1[0][0]);

        for(int row = 0 ; row < list1.length ; row++){
            for(int column = 0 ; column < list1[row].length ; column ++){ 
                System.out.println(" แถวที่ "+row+" คอลัม "+column+" ค่าเก็บ "+list1[row][column]);
            }
        }
    } 
}
