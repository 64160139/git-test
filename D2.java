import java.util.Scanner;
public class D2 {
    public static void main(String[] args) {
        // เปรียบเทียบตัวเลข
        Scanner sc = new Scanner(System.in);
        System.out.print("กรอกเลขตัวที่ 1 = ");
        int num1 = sc.nextInt();
        
        String re = ""; 
        
        /* ตัวอย่างเงื่อนไขแบบเก่า
        if(num1 % 2 ==0){
            System.out.println(" เลขคู่ ");
        }else{
            System.out.println(" เลขคี่ ");
        }
        */
        // เขียนย่อเงื่อนไขให้สั้นลง
        // if      ตัวแปร (เงื่อนไข) ? ถ้าเป็นจริงใส่คำสั่งหลัง ? 
        // else if ตัวแปร (เงื่อนไข) : ถ้าเป็นเท็จใส่คำสั่งหลัง :  
        re = (num1 % 2 ==0) ? num1+ " = เลขคู่ " : num1+ " = เลขคี่ " ;
        System.out.println(re);
    }   
    
}
